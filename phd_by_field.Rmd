---
title: "PhDs Awarded by Field"
output: html_notebook
---

```{r}
library(tidyverse)
library(ghibli)

my_font <- "Comfortaa"
my_bkgd <- "#f5f5f2"
my_theme <- theme(text = element_text(family = my_font, color = "#22211d"),
                  rect = element_rect(fill = my_bkgd),
                  plot.background = element_rect(fill = my_bkgd, color = NA),
                  panel.background = element_rect(fill = my_bkgd, color = NA),
                  panel.border = element_blank(),
                  legend.background = element_rect(fill = my_bkgd, color = NA),
                  legend.key = element_rect(fill = my_bkgd),
                  plot.caption = element_text(size = 6))

theme_set(theme_light() + my_theme)

```

```{r}

phd_by_field <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-02-19/phd_by_field.csv")

phd_by_field %>%
  filter(year == 2017) %>%
  count(broad_field, sort = T)

```

```{r}
glimpse(phd_by_field)

phd_by_field$year <- as.integer(phd_by_field$year)

```

```{r}

phd_by_field %>%
  ggplot(aes(year,n_phds, group = year)) +
  geom_boxplot() +
  scale_y_log10()


```

```{r}
phd_by_field %>%
  group_by(year) %>%
  filter(n_phds > 4000)

```

```{r}

phd_by_field %>% 
  group_by(year, broad_field) %>%
  mutate(n = sum(n_phds, na.rm = T)) %>%
  ungroup() %>%
  mutate(broad_field = str_replace(broad_field, " and ", " & "),
         broad_field = str_replace(broad_field, "sciences", "sci"),
         broad_field = str_replace(broad_field, "computer", "comp"),
         broad_field = str_to_title(broad_field)) %>%
  ggplot(aes(year,n,col = broad_field, fill = broad_field)) +
  geom_line(linetype = "dotted", show.legend = F) +
  geom_point(show.legend = T, 
             shape = 21, col = my_bkgd,
             size = 4) +
  scale_x_continuous(breaks = seq(2008,2017,1)) +
  scale_y_log10(breaks = c(0, 2000, 3000, 5000, 10000, 20000), labels = scales::comma_format()) +
  #expand_limits(y=0) +
  scale_color_manual(values = ghibli_palettes$MononokeMedium, name = "") +
  scale_fill_manual(values = ghibli_palettes$MononokeMedium, name = "") +
  theme(legend.position = "top") +
  guides(col = guide_legend(title.position = "top", direction = "horizontal", nrow = 2,
                            label.theme = element_text(size = 7, family = my_font))) +
  labs(x = "", y = "(in log scale)",
       title = "Number of PhD's Awarded By Broad Field",
       caption = "Source: National Science Foundation")

ggsave("broad_field.png")
```


```{r}

phd_by_field %>% 
  group_by(year) %>%
  top_n(4) %>%
  mutate(field = str_to_title(field)) %>%
  ggplot(aes(year,n_phds,col=field)) +
  geom_linerange(aes(ymin = 0, ymax = n_phds), position = position_dodge(width = 0.7), show.legend = F) +
  geom_point(position = position_dodge(width = 0.7), size = 3) +
  scale_color_manual(values = ghibli_palettes$PonyoMedium, name = "") +
  scale_x_continuous(breaks = seq(2008,2017,1)) +
  scale_y_continuous(breaks = seq(0,5000,1000), labels = scales::comma_format()) +
  theme(legend.position = "top") +
  guides(col = guide_legend(title.position = "left", direction = "horizontal", nrow = 2,
                            label.theme = element_text(size = 8, family = my_font))) +
  labs(x = "", y = "",
       title = "Top 4 Fields Annually, By Number of PhD's Awarded",
       caption = "Source: National Science Foundation")

ggsave("top4.png")

```

